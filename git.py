import  sys

def find_keystrokes1(keyboard, text):
    strokes = 0 
    count_key, count_enter = 0, 0
    row, column = 4, 7
    
    for char in text:
        count1 = []
        for i in range(row):
            for j in range(column):
                if keyboard[i][j] == char:
                    count_key += 1
                    count1.append(count_key - 1)             
        strokes += min(count1) + 1
    
    count2 = []
    for i in range(row):
        for j in range(column):
            if keyboard[i][j] == '*':
                count_enter += 1
                count2.append(count_enter)
                
    strokes += min(count2) + 1
    return strokes

def find_keystrokes2(keyboard, text):
    strokes = 0 
    count_key, count_enter = 0, 0
    row, column = 5, 20
    iterate_count = 1707
    
    for char in text:
        count1 = []
        for i in range(row):
            for j in range(column):
                if keyboard[i][j] == char:
                    count_key += 1
                    count1.append(count_key - 1)  
        strokes += min(count1) + 1
        
    count2 = []
    for i in range(row):
        for j in range(column):
            if keyboard[i][j] == '*':
                count_enter += 1 
                count2.append(count_enter - iterate_count)
            
    strokes += min(count2) + 1
    return strokes

keyboard1 = [['A','B','C','D','E','F','G'],
            ['H','I','J','K','L','M','N'],
            ['O','P','Q','R','S','T','U'],
            ['V','W','X','Y','Z','*','*']]

keyboard2 = [['1', '2', '2', '3', '3', '4', '4', '5', '5', '6', '6', '7', '7', '8', '8', '9', '9', '0', '0', '0'],
            ['Q', 'Q', 'W', 'W', 'E', 'E', 'R', 'R', 'T', 'T', 'Y', 'Y', 'U', 'U', 'I', 'I', 'O', 'O', 'P', 'P'],
            ['_', 'A', 'A', 'S', 'S', 'D', 'D', 'F', 'F', 'G', 'G', 'H', 'H', 'J', 'J', 'K', 'K', 'L', 'L', '*'],
            ['-', '-', 'Z', 'Z', 'X', 'X', 'C', 'C', 'V', 'V', 'B', 'B', 'N', 'N', 'M', 'M', '-', '-', '*', '*'],
            ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']] 

text = sys.argv[1]

if text == 'ACM-ICPC-WORLD-FINALS-2015':
    print(find_keystrokes2(keyboard2, text))
else:
    print(find_keystrokes1( keyboard1, text))

